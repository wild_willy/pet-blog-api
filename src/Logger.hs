{-# LANGUAGE OverloadedStrings #-}

module Logger
  ( adapt
  , prodLogEnv
  , logMsg
  , runKatipT
  , KatipT(..)
  , Katip(..)
  , LogEnv
  , Severity(..)
  ) where

import Control.Monad.Logger
import qualified Control.Monad.Logger as Logger
import Katip
import qualified System.IO as IO
import qualified System.Log.FastLogger as FastLogger

-- | Production logging environment.
prodLogEnv :: IO LogEnv
prodLogEnv = do
  hScribe <- mkHandleScribe ColorIfTerminal IO.stdout (permitItem DebugS) V2
  env <- initLogEnv "pet-blog-api" "production"
  registerScribe "stdout" hScribe defaultScribeSettings env

-- | Transforms fast logger LogLevel to Katip's Severity
fromLevel :: LogLevel -> Severity
fromLevel LevelDebug = DebugS
fromLevel LevelInfo = InfoS
fromLevel LevelWarn = WarningS
fromLevel LevelError = ErrorS
fromLevel (LevelOther _) = NoticeS

-- | Transforms Katip logMsg into monadLoggerLog to be used inside
-- MonadLogger monad
adapt ::
     (ToLogStr msg, Applicative m, Katip m)
  => (Namespace -> Severity -> Katip.LogStr -> m ())
  -> Loc
  -> LogSource
  -> LogLevel
  -> msg
  -> m ()
adapt f _ src lvl msg = f ns (fromLevel lvl) $ logStr' msg
  where
    ns = Namespace [src]
    logStr' = Katip.logStr . FastLogger.fromLogStr . Logger.toLogStr
