{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE StandaloneDeriving         #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Models where

import           App                         (AppCTX, dbPool)
import           Control.Monad.Reader        (MonadIO, MonadReader, asks,
                                              liftIO)
import           Data.Aeson
import qualified Data.ByteString.Lazy.Char8  as B
import           Data.Password.Instances
import           Data.Text                   (Text)
import           Data.Time                   (UTCTime)
import           Database.Persist.Postgresql (mockMigration)
import           Database.Persist.Sql        (PersistFieldSql, SqlPersistT,
                                              runMigration, runSqlPool)
import           Database.Persist.TH         (mkMigrate, mkPersist,
                                              persistLowerCase, share,
                                              sqlSettings)

share
  [mkPersist sqlSettings, mkMigrate "migrateAll"]
  [persistLowerCase|
  User json
    name Text
    email Text
    pass PassHash
    username Text
    UniqueUserEmail email
    UniqueUserName username
  Micropost json
    Id
    author UserId
    post Text
    posted UTCTime
    public Bool
  Subscription json
    subscriber UserId
    subscribedTo UserId
    createdAt UTCTime
    UniqueS subscriber subscribedTo|]

deriving instance Show Subscription

deriving instance Show Micropost

deriving instance Show User

deriving instance Eq User

deriving instance Eq Micropost

deriving instance Eq Subscription

instance ToJSON PassHash where
  toJSON (PassHash pass) = String pass

instance FromJSON PassHash where
  parseJSON = withText "PassHash" $ \x -> return $ PassHash x

-- | Mock migrations
mock :: IO ()
mock = mockMigration migrateAll

doMigrations :: SqlPersistT IO ()
doMigrations = runMigration migrateAll

-- | Run database actions on application context
runDb :: (MonadReader AppCTX m, MonadIO m) => SqlPersistT IO b -> m b
runDb query = do
  pool <- asks dbPool
  liftIO $ runSqlPool query pool
