{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}

module Lib where

import Api
import App
import Data.Aeson
import Data.Aeson.TH
import Network.Wai
import Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.RequestLogger.JSON
import Servant
import Servant.Auth.Server as SAS

jsonRequestLogger :: IO Middleware
jsonRequestLogger =
  mkRequestLogger $
  def {outputFormat = CustomOutputFormatWithDetails formatAsJSON}

startApp :: IO ()
startApp = do
  appCTX <- getCTX
  myKey <- generateKey
  warpLogger <- jsonRequestLogger
  let warpSettings = Warp.defaultSettings
      portSettings = Warp.setPort (appPort appCTX) warpSettings
      settings = Warp.setTimeout 55 portSettings
      jwtCfg = defaultJWTSettings myKey
      cookieCfg = defaultCookieSettings {cookieIsSecure = SAS.NotSecure}
      webCTX = cookieCfg :. jwtCfg :. EmptyContext
  Warp.runSettings settings $ warpLogger $ mkApp webCTX cookieCfg jwtCfg appCTX
