{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Api where

import           Api.Error
import           Api.Micropost
import           Api.Subscription
import           Api.User
import           App                           (AppCTX (..), AppT (..))
import           Control.Monad.Reader          (MonadIO, runReaderT)
import           Control.Monad.Trans.Bifunctor (firstT)
import           Data.Bifunctor                (first)
import           Servant
import           Servant.Auth                  as SA
import           Servant.Auth.Server           as SAS
import           Servant.Server

-- | Join all the APIs into one
type Api auths =
  UserApi auths :<|> MicropostApi auths :<|> SubscriptionApi auths

-- | Proxy of the api
api :: Proxy (Api '[JWT])
api = Proxy

-- | API Server
apiServer ::
  MonadIO m =>
  SAS.CookieSettings ->
  SAS.JWTSettings ->
  ServerT (Api auths) (AppT m)
apiServer cs jwts =
  userServer cs jwts :<|> micropostServer :<|> subscriptionServer

-- | Creates our Application (Servant)
mkApp ::
  Context '[SAS.CookieSettings, SAS.JWTSettings] ->
  CookieSettings ->
  JWTSettings ->
  AppCTX ->
  Application
mkApp webCTX cs jwts appCTX =
  serveWithContext api webCTX $
    hoistServerWithContext
      api
      (Proxy :: Proxy '[SAS.CookieSettings, SAS.JWTSettings])
      (Handler . firstT toServerError . flip runReaderT appCTX . runApp)
      (apiServer cs jwts)
