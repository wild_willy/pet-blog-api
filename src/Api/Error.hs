{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}

module Api.Error where

import Control.Exception
import Data.ByteString.Lazy.Char8 (ByteString)
import Data.Text (Text)
import GHC.Generics
import Servant.Server.Internal.ServerError

class ErrorText e where
  toErrorText :: e -> ByteString

class ToServerError e where
  toServerError :: e -> ServerError

-- | Api Error
data ApiError
  = UsernameNotFound
  | EmailNotFound
  | InvalidCredentials
  | EmailAlreadyExists
  | UsernameAlreadyExists
  | AuthenticationFailed
  | UserNotFound
  | MicropostNotFound
  deriving (Show, Generic, Eq)

instance ErrorText ApiError where
  toErrorText UsernameNotFound = "Username not registered"
  toErrorText EmailNotFound = "User with given email not found"
  toErrorText InvalidCredentials = "The provided credentials are invalid"
  toErrorText EmailAlreadyExists = "Provided email already registered"
  toErrorText UsernameAlreadyExists = "Provided username already in use"
  toErrorText AuthenticationFailed = "User email and password are incorrect"
  toErrorText UserNotFound = "User with given id not found"
  toErrorText MicropostNotFound = "Micropost not found"
  toErrorText _ = "Error message not implemented"

instance ToServerError ApiError where
  toServerError UsernameNotFound =
    err404 {errBody = toErrorText UsernameNotFound}
  toServerError EmailNotFound = err404 {errBody = toErrorText EmailNotFound}
  toServerError InvalidCredentials =
    err401 {errBody = toErrorText InvalidCredentials}
  toServerError EmailAlreadyExists =
    err400 {errBody = toErrorText EmailAlreadyExists}
  toServerError UsernameAlreadyExists =
    err400 {errBody = toErrorText UsernameAlreadyExists}
  toServerError UserNotFound = err404 {errBody = toErrorText UserNotFound}
  toServerError MicropostNotFound =
    err404 {errBody = toErrorText MicropostNotFound}
  toServerError a = err500 {errBody = toErrorText a}

instance Exception ApiError

-- | Application Error
data ApplicationError
  = UnexpectedError ByteString
  | APIError ApiError
  deriving (Show, Eq, Generic)

instance ErrorText ApplicationError where
  toErrorText (UnexpectedError err) = err
  toErrorText (APIError err) = toErrorText err

instance Exception ApplicationError

instance ToServerError ApplicationError where
  toServerError (UnexpectedError err) = err500 {errBody = err}
  toServerError (APIError err) = toServerError err
