{-# LANGUAGE DataKinds, TypeOperators #-}

module Api.User
  ( userServer
  , UserApi
  , userApi
  ) where

import Api.User.Handlers
import Api.User.Types
import App (AppT(..))
import Control.Monad.Except (MonadIO, lift, liftIO)
import Control.Monad.Logger (logDebugNS)
import Data.Aeson
import Data.Int (Int64)
import Data.Password.Instances
import Data.Text (Text)
import Servant
import Servant.Auth.Server as SAS

-- | User Server 
userServer ::
     (MonadIO m)
  => SAS.CookieSettings
  -> SAS.JWTSettings
  -> ServerT (UserApi auths) (AppT m)
userServer cs jwts =
  createUser :<|> loginHandler cs jwts :<|> adminHandler :<|> deleteByEmail :<|>
  updateUser
