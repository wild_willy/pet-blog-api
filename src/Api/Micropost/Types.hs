{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Api.Micropost.Types where

import           Api.User.Types      (AuthenticatedUser)
import           Data.Aeson
import           Data.Text           (Text)
import           Data.Time           (UTCTime)
import           Database.Persist
import           GHC.Generics
import           Models
import           Servant
import           Servant.Auth.Server as SAS

-- | New Micropost
data NewMicropost
  = NewMicropost
      { newPost :: Text,
        created :: UTCTime,
        public  :: Bool
      }
  deriving (Generic)

instance ToJSON NewMicropost

instance FromJSON NewMicropost

-- | Micropost API
type MicropostApi auths =
  (SAS.Auth auths AuthenticatedUser :> CreateMicropost)
    :<|> (SAS.Auth auths AuthenticatedUser :> DeleteMicropost)
    :<|> (SAS.Auth auths AuthenticatedUser :> GetMicropost)
    :<|> (SAS.Auth auths AuthenticatedUser :> GetMicropostsUser)
    :<|> (SAS.Auth auths AuthenticatedUser :> GetPublicMicroposts)
    :<|> (SAS.Auth auths AuthenticatedUser :> GetMicropostUserId)
    :<|> (SAS.Auth auths AuthenticatedUser :> UpdateMicropost)

-- | Micropost Creation API
type CreateMicropost =
  "micropost" :> "new" :> ReqBody '[JSON] NewMicropost :> Post '[JSON] MicropostId

-- | Micropost Delete API usage POST /micropost/:micropostId
type DeleteMicropost =
  "micropost" :> "delete" :> Capture "micropostId" MicropostId :> Post '[JSON] MicropostId

-- | Micropost Get API usage GET /micropost/:micropostId
type GetMicropost =
  "micropost" :> Capture "micropostId" MicropostId :> Get '[JSON] Micropost

-- | Get Microposts from user
type GetMicropostsUser =
  "micropost" :> "user" :> Capture "username" Text :> Get '[JSON] [Entity Micropost]

-- | Get Public Microposts
type GetPublicMicroposts =
  "micropost" :> "public" :> Get '[JSON] [Entity Micropost]

-- | Get Microposts by author's user Id
type GetMicropostUserId =
  "micropost" :> "author" :> Capture "authorId" UserId :> Get '[JSON] [Entity Micropost]

-- | Update the given Microposts text
type UpdateMicropost =
  "micropost" :> "update" :> Capture "micropostId" MicropostId :> ReqBody '[JSON] NewMicropost :> Post '[JSON] MicropostId

-- | micropost Proxy
micropostApi :: Proxy (MicropostApi '[JWT])
micropostApi = Proxy
