{-# LANGUAGE OverloadedStrings #-}

module Api.Micropost.Handlers where

import           Api.Error
import           Api.Micropost.Types
import           Api.User.Types
import           App                         (AppT (..))
import           Control.Monad.Logger        (logDebugNS)
import           Control.Monad.Reader        (MonadIO)
import           Data.Text                   (Text)
import           Database.Persist.Postgresql
import           Models
import           Servant
import           Servant.Auth.Server         as SAS
import           Servant.Server

-- | Creates a new micropost linked to the authenticated user
createMicropost ::
  MonadIO m =>
  AuthResult AuthenticatedUser ->
  NewMicropost ->
  AppT m MicropostId
createMicropost (SAS.Authenticated authUser) (NewMicropost post created public) = do
  logDebugNS "web" "creating a new micropost"
  let userEmail = authEmail authUser
  mUser <- runDb . getBy $ UniqueUserEmail userEmail
  case mUser of
    Nothing -> throwError $ APIError EmailNotFound
    Just user -> runDb . insert $ Micropost (entityKey user) post created public
createMicropost _ _ = throwError $ APIError InvalidCredentials

-- | Delete micropost from linked to the authenticated user
-- returns the id of the deleted micropost
deleteMicropost ::
  MonadIO m =>
  AuthResult AuthenticatedUser ->
  MicropostId ->
  AppT m MicropostId
deleteMicropost (SAS.Authenticated authUser) mId = do
  logDebugNS "web" "deleting a micropost"
  mMicropost <- runDb $ get mId
  maybe
    (throwError $ APIError MicropostNotFound)
    ( \post ->
        if micropostAuthor post == authUserId authUser
          then do
            runDb $ delete mId
            return mId
          else throwError $ APIError InvalidCredentials
    )
    mMicropost
deleteMicropost _ _ = throwError $ APIError InvalidCredentials

-- | Get's a Micropost
getMicropost ::
  MonadIO m =>
  AuthResult AuthenticatedUser ->
  MicropostId ->
  AppT m Micropost
getMicropost (SAS.Authenticated authUser) mId = do
  logDebugNS "web" "getting a micropost"
  mMicro <- runDb $ get mId
  case mMicro of
    Nothing    -> throwError $ APIError MicropostNotFound
    Just micro -> return micro
getMicropost _ _ = throwError $ APIError InvalidCredentials

-- | Get all micropost linked to a username
getMicropostsUser ::
  MonadIO m =>
  AuthResult AuthenticatedUser ->
  Text ->
  AppT m [Entity Micropost]
getMicropostsUser (SAS.Authenticated authUser) userName = do
  logDebugNS "web" "get all micropost from user name"
  mUser <- runDb . getBy $ UniqueUserName userName
  case mUser of
    Nothing   -> throwError $ APIError UsernameNotFound
    Just user -> runDb $ selectList [MicropostAuthor ==. entityKey user] []
getMicropostsUser _ _ = throwError $ APIError InvalidCredentials

-- | Get the latest Microposts that are public
getPublicMicroposts ::
  MonadIO m => AuthResult AuthenticatedUser -> AppT m [Entity Micropost]
getPublicMicroposts (SAS.Authenticated _) = do
  logDebugNS "web" "get the latest microposts that are public"
  runDb . selectList [MicropostPublic ==. True] $ [Desc MicropostPosted]
getPublicMicroposts _ = throwError $ APIError InvalidCredentials

-- | Get the given user's microposts
getMicropostsUserId ::
  MonadIO m =>
  AuthResult AuthenticatedUser ->
  UserId ->
  AppT m [Entity Micropost]
getMicropostsUserId (SAS.Authenticated _) authorId = do
  logDebugNS "web" "get the latest microposts from the user"
  runDb . selectList [MicropostAuthor ==. authorId] $ [Desc MicropostPosted]
getMicropostsUserId _ _ = throwError $ APIError InvalidCredentials

-- | Updates the given Micropost
updateMicropost ::
  MonadIO m =>
  AuthResult AuthenticatedUser ->
  MicropostId ->
  NewMicropost ->
  AppT m MicropostId
updateMicropost (SAS.Authenticated authUser) mId (NewMicropost p c pp) = do
  logDebugNS "web" "update micropost"
  authId <- micropostAuthor <$> runDb (getJust mId)
  if authId == authUserId authUser
    then do
      runDb $ update mId [MicropostPost =. p, MicropostPosted =. c, MicropostPublic =. pp]
      return mId
    else throwError $ APIError InvalidCredentials
updateMicropost _ _ _ = throwError $ APIError InvalidCredentials
