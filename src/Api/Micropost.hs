{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Api.Micropost
  ( MicropostApi,
    micropostApi,
    micropostServer,
  )
where

import           Api.Micropost.Handlers
import           Api.Micropost.Types
import           App                     (AppT (..))
import           Control.Monad.Except    (MonadIO, lift, liftIO)
import           Control.Monad.Logger    (logDebugNS)
import           Data.Aeson
import           Data.Int                (Int64)
import           Data.Password.Instances
import           Data.Text               (Text)
import           Servant
import           Servant.Auth.Server     as SAS

micropostServer :: MonadIO m => ServerT (MicropostApi auths) (AppT m)
micropostServer =
  createMicropost
    :<|> deleteMicropost
    :<|> getMicropost
    :<|> getMicropostsUser
    :<|> getPublicMicroposts
    :<|> getMicropostsUserId
    :<|> updateMicropost
