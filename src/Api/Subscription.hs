{-# LANGUAGE DataKinds, TypeOperators #-}

module Api.Subscription
  ( SubscriptionApi
  , subscriptionApi
  , subscriptionServer
  ) where

import Api.Subscription.Handlers
import Api.Subscription.Types
import App (AppT(..))
import Control.Monad.Except (MonadIO, lift, liftIO)
import Control.Monad.Logger (logDebugNS)
import Data.Aeson
import Data.Int (Int64)
import Data.Password.Instances
import Data.Text (Text)
import Servant
import Servant.Auth.Server as SAS

subscriptionServer :: MonadIO m => ServerT (SubscriptionApi auths) (AppT m)
subscriptionServer =
  createSubscription :<|> deleteSubscription :<|> getSubscriptions
