{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.User.Handlers where

import           Api.Error
import           Api.User.Types
import           App                         (AppT (..))
import           Control.Monad.Except        (MonadIO, lift, liftIO)
import           Control.Monad.Logger        (logDebugNS)
import           Data.Aeson
import           Data.Password.Instances
import           Data.Text                   (Text)
import           Data.Time                   (getCurrentTime)
import           Database.Persist.Postgresql (Entity (..), Key, deleteBy,
                                              fromSqlKey, get, getBy, insert,
                                              selectFirst, selectList, toSqlKey,
                                              updateWhere, (=.), (==.))
import           GHC.Generics
import           Models                      (EntityField (..),
                                              Subscription (..),
                                              Unique (UniqueUserEmail),
                                              User (..), UserId, runDb,
                                              userEmail, userName, userPass)
import           Servant
import           Servant.Auth                as SA
import           Servant.Auth.Server         as SAS

-- | Admin Handler retrieves all users.
adminHandler :: MonadIO m => AuthResult AuthenticatedUser -> AppT m [UserProfile]
adminHandler (SAS.Authenticated authUser) = do
  logDebugNS "web" "Admin user accesing"
  allUsers
adminHandler _ = throwError $ APIError InvalidCredentials

-- | Login auth Handler
loginHandler ::
  MonadIO m =>
  SAS.CookieSettings ->
  SAS.JWTSettings ->
  UserLoginForm ->
  AppT m (Headers '[Header "Set-Cookie" SetCookie, Header "Set-Cookie" SetCookie] AuthenticatedUser)
loginHandler cs jwts form = do
  usr <- authenticateUser form
  mApplyCookies <- liftIO $ SAS.acceptLogin cs jwts usr
  case mApplyCookies of
    Nothing -> do
      logDebugNS "web" "User Authetication failed"
      throwError $ APIError AuthenticationFailed
    Just applyCookies -> do
      logDebugNS "web" "User succesfully authenticated"
      pure $ applyCookies usr

-- | All users in the database
allUsers :: MonadIO m => AppT m [UserProfile]
allUsers = do
  logDebugNS "web" "allUsers"
  map extractUser <$> runDb (selectList [] [])

-- | Get User by email
getUserByEmail :: MonadIO m => Text -> AppT m User
getUserByEmail email = do
  logDebugNS "web" "getUserByEmail"
  mUser <- runDb . getBy $ UniqueUserEmail email
  case mUser of
    Nothing   -> throwError $ APIError EmailNotFound
    Just user -> return $ entityVal user

-- | Get User by Id.
getUserById :: MonadIO m => Key User -> AppT m User
getUserById userId = do
  logDebugNS "web" "getUserById"
  mUser <- runDb $ get userId
  case mUser of
    Nothing   -> throwError $ APIError UserNotFound
    Just user -> return user

-- | Creates a new user returns the user id and creates
-- a subcription to itself
createUser :: MonadIO m => NewUser -> AppT m (Key User)
createUser newUser = do
  logDebugNS "web" "creating a new user"
  hashedPass <- hashPass $ unhashedPass newUser
  userId <- runDb . insert $ User (name newUser) (email newUser) hashedPass (username newUser)
  time <- liftIO getCurrentTime
  runDb . insert $ Subscription userId userId time
  return userId

-- | From a login form return an authenticated user.
authenticateUser :: MonadIO m => UserLoginForm -> AppT m AuthenticatedUser
authenticateUser (UserLoginForm loginEmail loginPass) = do
  logDebugNS "web" "authenticating user"
  mUser <- runDb . getBy $ UniqueUserEmail loginEmail
  case mUser of
    Nothing -> throwError $ APIError EmailNotFound
    Just user ->
      let check = checkPass loginPass (userPass $ entityVal user)
          user' = entityVal user
          userId = entityKey user
       in case check of
            PassCheckSucc ->
              return $ AuthUser (userName user') (userEmail user') userId
            PassCheckFail -> throwError $ APIError AuthenticationFailed

-- | Delete user by email only Autheticated user.
deleteByEmail :: MonadIO m => AuthResult AuthenticatedUser -> AppT m ()
deleteByEmail (SAS.Authenticated usr) = do
  let email = authEmail usr
  logDebugNS "web" "deleting user"
  mUser <- runDb . getBy $ UniqueUserEmail email
  case mUser of
    Nothing   -> throwError $ APIError EmailNotFound
    Just user -> runDb . deleteBy $ UniqueUserEmail email
deleteByEmail _ = throwError $ APIError InvalidCredentials

-- | Update a users name or password
updateUser ::
  MonadIO m => AuthResult AuthenticatedUser -> UpdateUser -> AppT m ()
updateUser (SAS.Authenticated usr) upUser = do
  let email = authEmail usr
      mName = updName upUser
      mUnhashedPass = updPass upUser
  logDebugNS "web" "updating user"
  updateUserName email mName
  updateUserPass email mUnhashedPass
  return ()
updateUser _ _ = throwError $ APIError InvalidCredentials

-- | Update User's name
updateUserName :: MonadIO m => Text -> Maybe Text -> AppT m ()
updateUserName email Nothing = return ()
updateUserName email (Just newName) = do
  logDebugNS "web" "updating user's name"
  runDb $ updateWhere [UserEmail ==. email] [UserName =. newName]

-- | Update User's password
updateUserPass :: MonadIO m => Text -> Maybe Pass -> AppT m ()
updateUserPass email Nothing = return ()
updateUserPass email (Just newUnhashedPass) = do
  logDebugNS "web" "updating user's password"
  newHashedPass <- hashPass newUnhashedPass
  runDb $ updateWhere [UserEmail ==. email] [UserPass =. newHashedPass]
