{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Api.User.Types where

import           App                     (AppT (..))
import           Data.Aeson
import           Data.Password.Instances
import           Data.Text               (Text)
import           Database.Persist.Types  (Entity (..))
import           GHC.Generics
import           Models                  (Key (..), User (..))
import           Servant
import           Servant.Auth            as SA
import           Servant.Auth.Server     as SAS

instance ToJSON Pass where
  toJSON (Pass p) = String p

-- | User obtained from the web password is unhashed
-- for user creation only.
data NewUser
  = NewUser
      { name         :: Text,
        email        :: Text,
        unhashedPass :: Pass,
        username     :: Text
      }
  deriving (Generic)

instance FromJSON NewUser

instance ToJSON NewUser where
  toJSON (NewUser name email pass username) =
    object
      [ "name" .= name,
        "email" .= email,
        "unhashedPass" .= pass,
        "username" .= username
      ]

-- | Update User may have a name or password
data UpdateUser
  = UpdateUser
      { updName :: Maybe Text,
        updPass :: Maybe Pass
      }
  deriving (Generic)

instance FromJSON UpdateUser

instance ToJSON UpdateUser where
  toJSON (UpdateUser (Just name) _) = object ["updName" .= name]

-- | User login form for authentication.
data UserLoginForm
  = UserLoginForm
      { loginEmail :: Text,
        loginPass  :: Pass
      }
  deriving (Generic)

instance FromJSON UserLoginForm

instance ToJSON UserLoginForm where
  toJSON (UserLoginForm email pass) =
    object ["loginEmail" .= email, "loginPass" .= pass]

-- | An authenticated User
data AuthenticatedUser
  = AuthUser
      { authName   :: Text,
        authEmail  :: Text,
        authUserId :: Key User
      }
  deriving (Generic)

instance FromJSON AuthenticatedUser

instance ToJSON AuthenticatedUser

instance SAS.FromJWT AuthenticatedUser

instance SAS.ToJWT AuthenticatedUser

-- | An User to show in a the friends
data UserProfile
  = UserProfile
      { profileUsername :: Text,
        profileUserId   :: Key User
      }
  deriving (Show, Generic, FromJSON, ToJSON)

-- | Extract the user profile from an Entity User
extractUser :: Entity User -> UserProfile
extractUser enUser = UserProfile (userUsername $ entityVal enUser) (entityKey enUser)

-- | User Api
type UserApi auths =
  "users" :> "new" :> ReqBody '[JSON] NewUser :> Post '[JSON] (Key User)
    :<|> AuthUserApi
    :<|> (SAS.Auth auths AuthenticatedUser :> AdminApi)
    :<|> (SAS.Auth auths AuthenticatedUser :> DeleteApi)
    :<|> (SAS.Auth auths AuthenticatedUser :> UpdateApi)

-- | Auth Api
type AuthUserApi =
  "users" :> "login" :> ReqBody '[JSON] UserLoginForm :> Post '[JSON] (Headers '[Header "Set-Cookie" SetCookie, Header "Set-Cookie" SetCookie] AuthenticatedUser)

-- | Admin Api
type AdminApi = "users" :> Get '[JSON] [UserProfile]

-- | Delete User API
type DeleteApi = "users" :> "delete" :> Post '[JSON] ()

-- | Update User Api
type UpdateApi =
  "users" :> "update" :> ReqBody '[JSON] UpdateUser :> Post '[JSON] ()

-- | User Api Proxy
userApi :: Proxy (UserApi '[JWT])
userApi = Proxy
