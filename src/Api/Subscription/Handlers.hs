{-# LANGUAGE OverloadedStrings #-}

module Api.Subscription.Handlers where

import           Api.Error
import           Api.Subscription.Types
import           Api.User.Types
import           App                         (AppT (..))
import           Control.Monad.Logger        (logDebugNS)
import           Control.Monad.Reader        (MonadIO, liftIO)
import           Data.Text                   (Text)
import           Data.Time
import           Database.Persist.Postgresql
import           Models
import           Servant
import           Servant.Auth.Server         as SAS
import           Servant.Server

-- | Handler for creating a new subscription
createSubscription ::
  MonadIO m =>
  AuthResult AuthenticatedUser ->
  Key User ->
  AppT m SubscriptionId
createSubscription (SAS.Authenticated user) subTo = do
  logDebugNS "web" "create a new subscription"
  time <- liftIO getCurrentTime
  let sub = Subscription (authUserId user) subTo time
  runDb $ insert sub
createSubscription _ _ = throwError $ APIError InvalidCredentials

-- | Handler for deleting a subscription
deleteSubscription ::
  MonadIO m => AuthResult AuthenticatedUser -> UserId -> AppT m ()
deleteSubscription (SAS.Authenticated user) uId = do
  logDebugNS "web" "delete a subscription to the given user id"
  if authUserId user == uId
    then return ()
    else runDb $ deleteWhere [SubscriptionSubscribedTo ==. uId, SubscriptionSubscriber ==. authUserId user]
deleteSubscription _ _ = throwError $ APIError InvalidCredentials

-- | Handler for getting the authenticated user's subscriptions
getSubscriptions ::
  MonadIO m => AuthResult AuthenticatedUser -> AppT m [Subscription]
getSubscriptions (SAS.Authenticated user) = do
  logDebugNS "web" "get subscriptions of the authenticated user "
  fmap (map entityVal)
    $ runDb
    $ selectList [SubscriptionSubscriber ==. authUserId user] []
getSubscriptions _ = throwError $ APIError InvalidCredentials
