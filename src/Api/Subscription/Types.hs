{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}

module Api.Subscription.Types where

import           Api.User.Types          (AuthenticatedUser (..))
import           App                     (AppT (..))
import           Data.Aeson
import           Data.Password.Instances
import           Data.Text               (Text)
import           GHC.Generics
import           Models
import           Servant
import           Servant.Auth            as SA
import           Servant.Auth.Server     as SAS

type SubscriptionApi auths =
  (SAS.Auth auths AuthenticatedUser :> CreateSubscription) :<|> (SAS.Auth auths AuthenticatedUser :> DeleteSubscription) :<|> (SAS.Auth auths AuthenticatedUser :> GetSubscriptions)

-- | Create a Subscription
type CreateSubscription =
  "subscription" :> "new" :> Capture ":userId" (Key User) :> Post '[JSON] SubscriptionId

-- | Delete a Subscription to the given user id
type DeleteSubscription =
  "subscription" :> "delete" :> Capture "userId" UserId :> Post '[JSON] ()

-- | Get all subscription related to the authenticated user
type GetSubscriptions = "subscription" :> "get" :> Get '[JSON] [Subscription]

-- | Proxy
subscriptionApi :: Proxy (SubscriptionApi '[JSON])
subscriptionApi = Proxy
