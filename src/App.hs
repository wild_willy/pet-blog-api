{-# LANGUAGE GeneralizedNewtypeDeriving, FlexibleInstances,
  MultiParamTypeClasses, OverloadedStrings #-}

module App where

import Api.Error
import Control.Monad.Except (ExceptT, MonadError)
import Control.Monad.Logger (MonadLogger(..))
import Control.Monad.Reader (MonadIO, MonadReader, ReaderT, asks, liftIO)
import Data.Pool (Pool, destroyAllResources)
import Database.Persist.Postgresql
  ( ConnectionPool
  , SqlBackend
  , SqlPersistM
  , createPostgresqlPool
  )
import qualified Katip
import Logger
import Network.Wai (Middleware)
import Network.Wai.Handler.Warp (Port)
import Network.Wai.Middleware.RequestLogger (logStdout, logStdoutDev)
import Servant
import Servant.Server (ServerError)
import System.Environment (lookupEnv)
import Text.Read

newtype AppT m a =
  AppT
    { runApp :: ReaderT AppCTX (ExceptT ApplicationError m) a
    }
  deriving ( Functor
           , Applicative
           , Monad
           , MonadReader AppCTX
           , MonadError ApplicationError
           , MonadIO
           )

-- | Type Alias for application monad stack over IO
type AppM a = AppT IO a

-- | Application's running environment
data Environment
  = Development
  | Staging
  | Production
  deriving (Eq, Show, Read)

-- | The context of our application. 
data AppCTX =
  AppCTX
    { dbPool :: ConnectionPool
    , appEnv :: Environment
    , logEnv :: LogEnv
    , appPort :: Port
    }

-- | Katip instance
--
instance MonadIO m => Katip (AppT m) where
  getLogEnv = asks logEnv
  localLogEnv = error "local log env not implemented"

-- | MonadLogger instance
instance MonadIO m => MonadLogger (AppT m) where
  monadLoggerLog = adapt logMsg

-- | MonadLogger instance KatipT
instance MonadIO m => MonadLogger (KatipT m) where
  monadLoggerLog = adapt logMsg

-- | Returns a Middleware in accordance
setLog :: Environment -> Middleware
setLog Staging = id
setLog Development = logStdoutDev
setLog Production = logStdout

-- | Web request logger (inspiration from ApacheLogger wai-logger)
katipLogger :: LogEnv -> Middleware
katipLogger env app req res =
  runKatipT env $ do
    logMsg "web" InfoS "todo: received some request"
    liftIO $ app req res

-- | Creates a ConnectionPool for the given environment.
createPool :: Environment -> LogEnv -> IO ConnectionPool
createPool Staging env =
  runKatipT env $ createPostgresqlPool "host=localhost dbname=testdb" 1
createPool Development env =
  runKatipT env $ createPostgresqlPool "host=localhost dbname=testdb" 1
createPool Production env =
  runKatipT env $ createPostgresqlPool "host=localhost dbname=testdb" 2

-- | Looks up a setting in the environment, with a provided default, and
-- 'read's that information into the inferred type.
lookupSetting :: Read a => String -> a -> IO a
lookupSetting env def = do
  maybeValue <- lookupEnv env
  case maybeValue of
    Nothing -> return def
    Just str -> maybe (handleFailedRead str) return (readMaybe str)
  where
    handleFailedRead str =
      error $
      mconcat ["Failed to read [[", str, "]] for environment variable ", env]

-- | Get applications context.
getCTX :: IO AppCTX
getCTX = do
  port <- lookupSetting "PORT" 8080
  env <- lookupSetting "ENV" Development
  log <- prodLogEnv
  pool <- createPool env log
  pure $ AppCTX pool env log port

-- | Clean the application context
shutdownApp :: AppCTX -> IO ()
shutdownApp cfg = do
  Katip.closeScribes $ logEnv cfg
  destroyAllResources $ dbPool cfg
  pure ()
-- | Creates the WAI application
-- initApp :: AppCTX -> IO Application
-- initApp cfg = do
--  let logger = setLog $ appEnv cfg
--  pure . logger . app $ cfg
