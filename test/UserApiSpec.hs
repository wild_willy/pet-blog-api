{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module UserApiSpec where

import Api.Error
import Api.User
import Api.User.Types
import App
import qualified Control.Concurrent as C
import Control.Exception (bracket, throwIO)
import Control.Monad.Except (ExceptT, lift, runExceptT)
import Control.Monad.Reader (MonadIO, runReaderT)
import Data.ByteString.Char8 as B
import Data.Either (fromRight, isLeft, isRight)
import Data.List.Split (endBy)
import Data.Password.Instances
import Data.Text
import Data.Time (getCurrentTime)
import Database.Persist.Postgresql
import Lib (startApp)
import Models
import Network.HTTP.Client hiding (Proxy)
import Network.HTTP.Types
import Network.Wai
import qualified Network.Wai.Handler.Warp as Warp
import Servant
import Servant.Auth as SA
import Servant.Auth.Client
import Servant.Auth.Server as SAS
import Servant.Client
import Servant.Server.Internal.ServerError
import Test.Hspec
import Test.Hspec.Core.Spec
import Test.Hspec.Wai
import Test.Hspec.Wai.JSON
import TestUtils

spec :: Spec
spec =
  sequential $ do
    keys <- runIO $ appToIO setupDb
    let createUser :<|> login :<|> getUsers :<|> deleteUser :<|> updateUser =
          client (Proxy :: Proxy (UserApi '[ JWT]))
    baseUrl <- runIO $ parseBaseUrl "http://localhost:8080"
    manager <- runIO $ newManager defaultManagerSettings
    let clientEnv = mkClientEnv manager baseUrl
    describe "Users API" $ do
      it "should return a jwt login" $ do
        res <- runClientM (login (UserLoginForm "ex@ex.com" "pass1")) clientEnv
        shouldBe (isRight res) True
      it "should create an user" $ do
        res <-
          runClientM
            (createUser (NewUser "Jose" "ex3@ex.com" "pass3" "joseson"))
            clientEnv
        shouldBe res (Right $ toSqlKey 3)
      it "should return users" $ do
        token <-
          TestUtils.getToken <$>
          runClientM (login (UserLoginForm "ex@ex.com" "pass1")) clientEnv
        res2 <- runClientM (getUsers token) clientEnv
        shouldSatisfy res2 (not . Prelude.null . fromRight [])
      context "wrong credentials" $ do
        it "should return authentication error" $ do
          res <-
            runClientM (login (UserLoginForm "ex@ex.com" "nopass")) clientEnv
          let expectedResponse = toServerError $ APIError AuthenticationFailed
          shouldBe True (compareFailureResponse res expectedResponse)
        it "should return invalid credentials error" $ do
          res <- runClientM (getUsers (Token "error")) clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
