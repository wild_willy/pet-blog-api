module Main where

import qualified Control.Concurrent as C
import Lib (startApp)
import qualified MicropostApiSpec as M
import qualified SubscriptionApiSpec as S
import Test.Hspec
import Test.Hspec.Core.Spec
import Test.Hspec.Wai
import Test.Hspec.Wai.JSON
import TestUtils (appToIO, setupDb)
import qualified UserApiSpec as U

ignoreContext = aroundWith (\actionRunner -> const (actionRunner ()))

main :: IO ()
main =
  hspec $
  beforeAll (liftIO $ C.forkIO startApp) . afterAll (C.killThread) $
  sequential $
  ignoreContext $ do
    S.spec
    U.spec
    M.spec
