{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module SubscriptionApiSpec where

import           Api.Error
import           Api.Subscription
import           Api.Subscription.Types
import           Api.User
import           Api.User.Types
import           App
import qualified Control.Concurrent          as C
import           Control.Exception           (bracket, throwIO)
import           Control.Monad.Except        (ExceptT, lift, runExceptT)
import           Control.Monad.Reader        (MonadIO, runReaderT)
import qualified Data.ByteString.Char8       as B
import           Data.Either                 (isLeft, isRight)
import           Data.List.Split             (endBy)
import           Data.Password.Instances
import           Data.Text                   (Text)
import           Data.Time                   (getCurrentTime)
import           Database.Persist.Postgresql
import           Lib                         (startApp)
import           Models
import           Network.HTTP.Client         hiding (Proxy)
import           Network.HTTP.Types
import           Network.Wai
import qualified Network.Wai.Handler.Warp    as Warp
import           Servant
import           Servant.Auth                as SA
import           Servant.Auth.Client
import           Servant.Auth.Server         as SAS
import           Servant.Client
import           Test.Hspec
import           Test.Hspec.Core.Spec
import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON
import           TestUtils

createUser :<|> login :<|> getUsers :<|> deleteUser :<|> updateUser =
  client (Proxy :: Proxy (UserApi '[JWT]))

createSubscription :<|> deleteSubscription :<|> getSubscriptions =
  client (Proxy :: Proxy (SubscriptionApi '[JWT]))

data TestEnv
  = TestEnv
      { keys      :: [Key User],
        clientEnv :: ClientEnv,
        token     :: Token
      }

mkTestEnv :: IO TestEnv
mkTestEnv = do
  keys <- appToIO setupDb
  baseUrl <- parseBaseUrl "http://localhost:8080"
  manager <- newManager defaultManagerSettings
  let clientEnv = mkClientEnv manager baseUrl
  token <-
    TestUtils.getToken
      <$> runClientM (login (UserLoginForm "ex2@ex.com" "pass2")) clientEnv
  return $ TestEnv keys clientEnv token

spec =
  sequential
    $ beforeAll mkTestEnv
    $ describe "Subscriptions Api"
    $ do
      it "should create subscription" $ \(TestEnv [userId, userId2] clientEnv token) -> do
        time <- getCurrentTime
        res <-
          runClientM
            (createSubscription token userId)
            clientEnv
        isRight res `shouldBe` True
      it "should delete subscription" $ \(TestEnv [userId, userId2] clientEnv token) -> do
        res <- runClientM (deleteSubscription token userId) clientEnv
        isRight res `shouldBe` True
      it "should get all subscriptions" $ \(TestEnv [userId, userId2] clientEnv token) -> do
        res <- runClientM (getSubscriptions token) clientEnv
        isRight res `shouldBe` True
      context "when provided with wrong credentials" $ do
        it "should return invalid credential for subscription creation" $ \(TestEnv [userId, userId2] clientEnv token) -> do
          time <- getCurrentTime
          res <-
            runClientM
              ( createSubscription
                  (Token "error")
                  userId
              )
              clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
        it "should return invalid credentials error for subscription delete" $ \(TestEnv [userId, userId2] clientEnv token) -> do
          res <- runClientM (deleteSubscription (Token "error") userId) clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
        it "should return invalid credentials error for getting all subscriptions" $ \(TestEnv [userId, userId2] clientEnv token) -> do
          res <- runClientM (getSubscriptions (Token "error")) clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
