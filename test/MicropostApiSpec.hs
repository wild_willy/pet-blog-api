{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

module MicropostApiSpec where

import           Api.Error
import           Api.Micropost
import           Api.Micropost.Types
import           Api.User
import           Api.User.Types
import           App
import qualified Control.Concurrent          as C
import           Control.Exception           (bracket, throwIO)
import           Control.Monad.Except        (ExceptT, lift, runExceptT)
import           Control.Monad.Reader        (MonadIO, runReaderT)
import           Data.ByteString.Char8       as B
import           Data.Either                 (isLeft, isRight)
import           Data.Either                 (fromRight)
import           Data.List.Split             (endBy)
import           Data.Password.Instances
import           Data.Text
import           Data.Time                   (getCurrentTime)
import           Database.Persist.Postgresql
import           Lib                         (startApp)
import           Models
import           Network.HTTP.Client         hiding (Proxy)
import           Network.HTTP.Types
import           Network.Wai
import qualified Network.Wai.Handler.Warp    as Warp
import           Servant
import           Servant.Auth                as SA
import           Servant.Auth.Client
import           Servant.Auth.Server         as SAS
import           Servant.Client
import           Test.Hspec
import           Test.Hspec.Core.Spec
import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON
import           TestUtils

spec :: Spec
spec =
  sequential $ do
    let createUser :<|> login :<|> getUsers :<|> deleteUser :<|> updateUser =
          client (Proxy :: Proxy (UserApi '[JWT]))
    baseUrl <- runIO $ parseBaseUrl "http://localhost:8080"
    manager <- runIO $ newManager defaultManagerSettings
    let clientEnv = mkClientEnv manager baseUrl
    keys <- runIO $ appToIO setupDb
    let createMicropost :<|> deleteMicropost :<|> getMicropost :<|> getMicropostsUser :<|> getPublicMicroposts :<|> getMicropostsUserId :<|> updateMicropost =
          client (Proxy :: Proxy (MicropostApi '[JWT]))
    describe "Microposts API" $ do
      it "should create a Micropost" $ do
        token <-
          TestUtils.getToken
            <$> runClientM (login (UserLoginForm "ex2@ex.com" "pass2")) clientEnv
        time <- getCurrentTime
        res <-
          runClientM
            (createMicropost token (NewMicropost "primer micropost" time True))
            clientEnv
        shouldBe (Right $ toSqlKey 1) res
      it "should get a micropost by id" $ do
        token <-
          TestUtils.getToken
            <$> runClientM (login (UserLoginForm "ex@ex.com" "pass1")) clientEnv
        time <- getCurrentTime
        let newPost = NewMicropost "segundo post" time True
        res <- runClientM (createMicropost token newPost) clientEnv
        let expectedMicropost = Micropost (toSqlKey 1) "segundo post" time True
        res2 <- runClientM (getMicropost token (toSqlKey 2)) clientEnv
        shouldBe (Right expectedMicropost) res2
      it "should get all the users microposts" $ do
        token <-
          TestUtils.getToken
            <$> runClientM (login (UserLoginForm "ex@ex.com" "pass1")) clientEnv
        res2 <- runClientM (getMicropostsUser token "carlson") clientEnv
        shouldSatisfy res2 (not . Prelude.null . fromRight [])
      it "should delete a Micropost" $ do
        token <-
          TestUtils.getToken
            <$> runClientM (login (UserLoginForm "ex2@ex.com" "pass2")) clientEnv
        time <- getCurrentTime
        res2 <- runClientM (deleteMicropost token (toSqlKey 1)) clientEnv
        shouldBe res2 (Right $ toSqlKey 1)
      it "should give all public microposts" $ do
        token <-
          TestUtils.getToken
            <$> runClientM (login (UserLoginForm "ex@ex.com" "pass1")) clientEnv
        res2 <- runClientM (getPublicMicroposts token) clientEnv
        shouldBe (isRight res2) True
      it "should update the given micropost" $ do
        token <-
          TestUtils.getToken
            <$> runClientM (login (UserLoginForm "ex@ex.com" "pass1")) clientEnv
        time <- getCurrentTime
        res <- runClientM (getMicropost token (toSqlKey 2)) clientEnv
        res2 <- runClientM (updateMicropost token (toSqlKey 2) (NewMicropost "updated micropost" time True)) clientEnv
        shouldBe res2 (Right $ toSqlKey 2)
        res3 <- runClientM (getMicropost token (toSqlKey 2)) clientEnv
        shouldBe (res == res3) False
      context "when provided with wrong credentials" $ do
        it "should return invalid credentials error for micropost create" $ do
          time <- getCurrentTime
          let newPost = NewMicropost "segundo post" time True
          res <- runClientM (createMicropost (Token "error") newPost) clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
        it "should return invalid credentials error for get micropost id" $ do
          res <-
            runClientM (getMicropost (Token "error") (toSqlKey 1)) clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
        it "should return invalid credentials error for get micropost user" $ do
          res <-
            runClientM (getMicropostsUser (Token "error") "carlson") clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
        it "should return invalid credentials error for delete micropost" $ do
          res <-
            runClientM (deleteMicropost (Token "error") (toSqlKey 1)) clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
          token <- TestUtils.getToken <$> runClientM (login (UserLoginForm "ex2@ex.com" "pass2")) clientEnv
          res <- runClientM (deleteMicropost token (toSqlKey 2)) clientEnv
          shouldBe True (compareFailureResponse res expectedResponse)
        it "should return invalid credentials error for update micropost" $ do
          time <- getCurrentTime
          res <- runClientM (updateMicropost (Token "error") (toSqlKey 1) (NewMicropost "fjkadsnf" time True)) clientEnv
          let expectedResponse = toServerError $ APIError InvalidCredentials
          shouldBe True (compareFailureResponse res expectedResponse)
      context "when micropost does not exist" $ do
        it "should return micropost not found error for get micropost" $ do
          token <-
            TestUtils.getToken
              <$> runClientM (login (UserLoginForm "ex@ex.com" "pass1")) clientEnv
          res <- runClientM (getMicropost token (toSqlKey 4)) clientEnv
          let expectedResponse = toServerError $ APIError MicropostNotFound
          shouldBe True (compareFailureResponse res expectedResponse)
        it "should return micropost not found error for delete micropost" $ do
          token <-
            TestUtils.getToken
              <$> runClientM (login (UserLoginForm "ex@ex.com" "pass1")) clientEnv
          res <- runClientM (deleteMicropost token (toSqlKey 7)) clientEnv
          let expectedResponse = toServerError $ APIError MicropostNotFound
          shouldBe True (compareFailureResponse res expectedResponse)
