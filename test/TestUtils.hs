{-# LANGUAGE OverloadedStrings #-}

module TestUtils where

import           App                                 (AppT (..), getCTX)
import qualified Control.Concurrent                  as C
import           Control.Exception                   (bracket, throwIO)
import           Control.Monad.Except                (ExceptT, lift, liftIO,
                                                      runExceptT)
import           Control.Monad.Reader                (MonadIO, runReaderT)
import           Data.ByteString.Char8               as B
import           Data.List.Split                     (endBy)
import           Data.Password.Instances
import           Database.Persist.Postgresql
import           Lib                                 (startApp)
import           Models
import           Network.HTTP.Client                 hiding (Proxy)
import           Network.HTTP.Types
import           Network.Wai
import qualified Network.Wai.Handler.Warp            as Warp
import           Servant
import           Servant.Auth                        as SA
import           Servant.Auth.Client
import           Servant.Auth.Server                 as SAS
import           Servant.Client
import           Servant.Server.Internal.ServerError (responseServerError)
import           Test.Hspec
import           Test.Hspec.Wai
import           Test.Hspec.Wai.JSON

-- | Setups the database with example user data assumes db is already migrated
setupDb :: MonadIO m => AppT m [Key User]
setupDb = do
  hPass1 <- hashPass $ Pass "pass1"
  hPass2 <- hashPass $ Pass "pass2"
  runDb $ runMigration migrateAll
  runDb $ rawExecute "TRUNCATE public.user RESTART IDENTITY CASCADE" []
  userId1 <- runDb $ insert (User "carl" "ex@ex.com" hPass1 "carlson")
  userId2 <- runDb $ insert (User "paul" "ex2@ex.com" hPass2 "paulson")
  return [userId1, userId2]

-- | Transforms the AppT to IO
appToIO :: AppT IO a -> IO a
appToIO appT = do
  cfg <- getCTX
  res <- runExceptT $ runReaderT (runApp appT) cfg
  case res of
    Left err -> throwIO err
    Right a  -> return a

-- | With app
withApp :: IO () -> IO ()
withApp action =
  bracket (liftIO $ C.forkIO startApp) C.killThread (const action)

-- | Return the Token of a header
getToken headers =
  case headers of
    Left error -> Token "eero"
    Right h ->
      Token $
      B.pack $
      Prelude.drop 11 $
      Prelude.head $ endBy ";" . B.unpack $ snd . Prelude.head $ getHeaders h

-- | Compare a ServantError with a ClientError
compareFailureResponse :: Either ClientError a -> ServerError -> Bool
compareFailureResponse (Left (FailureResponse _ res)) sError =
  statusCode (responseStatusCode res) == errHTTPCode sError &&
  Servant.Client.responseBody res == errBody sError
compareFailureResponse _ _ = False
